﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using static BooksManager.BookHolder;

namespace BooksManager.Tests
{
    [TestClass()]
    public class BookHolderTests
    {
        [TestMethod()] //Проверка паттерна "Singleton"
        public void ValidateSingleton()
        {
            Assert.ReferenceEquals(BookHolder.Instance, BookHolder.Instance);
        }
        
        [TestMethod()] //Проверка инициализации
        public void AddInitializeTest()
        {
            Assert.IsFalse(BookHolder.Instance.GetBooks().Count > 0);
        }

        [TestMethod()] //Проверка валидности списка
        public void GetBooksTest()
        {
            Assert.IsTrue(BookHolder.Instance.GetBooks() != null);
        }

        [TestMethod()] //Проверка добавления книги
        public void AddBookTest()
        {
            ClearBooks();
            BookHolder.Book book = CreateTestBook();
            BookHolder.Instance.AddBook(book);
            Assert.AreEqual(BookHolder.Instance.GetBooks().Count, 1);
        }

        [TestMethod()] //Проверка ошибки добавления книги
        [ExpectedException(typeof(InvalidBookException))]
        public void AddBookFailTest()
        {
            BookHolder.Book book = new BookHolder.Book();
            BookHolder.Instance.AddBook(book);
        }

        [TestMethod()] //Проверка удаления книги
        public void RemoveBookTest()
        {
            ClearBooks();
            BookHolder.Book book = CreateTestBook();
            BookHolder.Instance.AddBook(book);
            Assert.AreEqual(BookHolder.Instance.GetBooks().Count, 1);
            BookHolder.Instance.RemoveBook(book);
            Assert.AreEqual(BookHolder.Instance.GetBooks().Count, 0);
        }

        [TestMethod()]
        public void BookValidationTest()
        {
            ClearBooks();
            BookHolder.Book book = new BookHolder.Book();
            Assert.IsFalse(BookHolder.Instance.IsValidBook(book));
            book.Year = "23.02.2016";
            Assert.IsFalse(BookHolder.Instance.IsValidBook(book));
            book.Link = "https://books.google.by/books?id=O7T3CwAAQBAJ&hl=ru&source=gbs_slider_cls_metadata_7_mylibrary&redir_esc=y";
            Assert.IsFalse(BookHolder.Instance.IsValidBook(book));
            book.Isbn = "1234-2345-3453-34534-1231";
            Assert.IsFalse(BookHolder.Instance.IsValidBook(book));
            book.Name = "The News Publisher Playbook";
            Assert.IsFalse(BookHolder.Instance.IsValidBook(book));
            book.Author = "Google Inc.";
            Assert.IsFalse(BookHolder.Instance.IsValidBook(book));
            book.Publisher = "Google Play Books";
            Assert.IsFalse(BookHolder.Instance.IsValidBook(book));
            book.Price = "Free";
            Assert.IsFalse(BookHolder.Instance.IsValidBook(book));
            book.Description = "As a news publisher, you know that many readers are switching from print to their smartphones and tablets to access news.";
            BookHolder.Instance.AddBook(book);
        }

        [TestMethod()] //Проверка валидности года книги
        public void YearValidationTest()
        {
            Assert.IsFalse(BookHolder.IsYearValid(null));
            Assert.IsFalse(BookHolder.IsYearValid("12.01023"));
            Assert.IsTrue(BookHolder.IsYearValid("23.10.2015"));
        }

        [TestMethod()] //Проверка валидности ссылки на книгу
        public void LinkValidationTest()
        {
            Assert.IsFalse(BookHolder.IsLinkValid(null));

            Assert.IsTrue(BookHolder.IsLinkValid("http://books.com"));
            Assert.IsFalse(BookHolder.IsLinkValid("htt://books.com"));

            Assert.IsTrue(BookHolder.IsLinkValid("https://books.com"));
            Assert.IsFalse(BookHolder.IsLinkValid("htps://books.com"));
        }

        [TestMethod()] //Проверка сортировки по имени
        public void TestNameSort()
        {
            ClearBooks();
            BookHolder.Instance.SetSortBy(SortBy.Name);

            Book firstBook = CreateTestBook();
            firstBook.Name = "1 " + firstBook.Name;

            Book secondBook = CreateTestBook();
            secondBook.Name = "2 " + secondBook.Name;

            BookHolder.Instance.AddBook(secondBook);
            BookHolder.Instance.AddBook(firstBook);

            List<Book> books = BookHolder.Instance.GetBooks();

            Assert.AreSame(books[0], firstBook);
            Assert.AreSame(books[1], secondBook);
        }

        [TestMethod] //Проверка сортировки по автору
        public void TestAuthorSort()
        {
            ClearBooks();
            BookHolder.Instance.GetBooks().Clear();
            BookHolder.Instance.SetSortBy(SortBy.Author);

            Book firstBook = CreateTestBook();
            firstBook.Author = "1 " + firstBook.Author;

            Book secondBook = CreateTestBook();
            secondBook.Author = "2 " + secondBook.Author;

            BookHolder.Instance.AddBook(secondBook);
            BookHolder.Instance.AddBook(firstBook);

            List<Book> books = BookHolder.Instance.GetBooks();

            Assert.AreSame(books[0], firstBook);
            Assert.AreSame(books[1], secondBook);
        }

        [TestMethod] //Проверка сортировки по цене
        public void TestPriceSort()
        {
            ClearBooks();
            BookHolder.Instance.GetBooks().Clear();
            BookHolder.Instance.SetSortBy(SortBy.Price);

            Book firstBook = CreateTestBook();
            firstBook.Price = "1 " + firstBook.Price;

            Book secondBook = CreateTestBook();
            secondBook.Price = "2 " + secondBook.Price;

            BookHolder.Instance.AddBook(secondBook);
            BookHolder.Instance.AddBook(firstBook);

            List<Book> books = BookHolder.Instance.GetBooks();

            Assert.AreSame(books[0], firstBook);
            Assert.AreSame(books[1], secondBook);
        }

        [TestMethod] //Проверка поиска
        public void TestSearch()
        {
            ClearBooks();
            Book book = CreateTestBook();

            Assert.AreEqual(BookHolder.Instance.Search(null).Count, 0);

            BookHolder.Instance.AddBook(book);

            Assert.AreEqual(BookHolder.Instance.Search(null).Count, 1);
            Assert.AreEqual(BookHolder.Instance.Search("a").Count, 1);
            Assert.AreEqual(BookHolder.Instance.Search("z").Count, 0);
        }

        [TestMethod] //Проверка оповощения об изменении списка книг
        public void TestNotifyListener()
        {
            ClearBooks();
            Book book = CreateTestBook();
            InterfaceTestHelper helper = new InterfaceTestHelper();
            BookHolder.Instance.RegisterListener(helper);
            BookHolder.Instance.AddBook(book);
            Assert.IsTrue(helper.IsNotified);

            ClearBooks();
            BookHolder.Instance.UnregisterListener(helper);
            helper = new InterfaceTestHelper();
            BookHolder.Instance.AddBook(book);
            Assert.IsFalse(helper.IsNotified);
        }

        private BookHolder.Book CreateTestBook()
        {
            BookHolder.Book book = new BookHolder.Book();
            book.Isbn = "1234-2345-3453-34534-1231";
            book.Name = "The News Publisher Playbook";
            book.Author = "Google Inc.";
            book.Publisher = "Google Play Books";
            book.Year = "23.02.2016";
            book.Link = "https://books.google.by/books?id=O7T3CwAAQBAJ&hl=ru&source=gbs_slider_cls_metadata_7_mylibrary&redir_esc=y";
            book.Price = "Free";
            book.Description = "As a news publisher, you know that many readers are switching from print to their smartphones and tablets to access news.";
            return book;
        }

        private void ClearBooks()
        {
            BookHolder.Instance.GetBooks().Clear();
        }

        private class InterfaceTestHelper : OnDataLoadRequired
        {
            public bool IsNotified { get; set; }

            public void OnDataLoadRequired()
            {
                IsNotified = true;
            }
        }

    }
}