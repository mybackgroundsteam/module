﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace BooksManager
{
    //Вспомогательны класс для хранения и обработки данных
    public class BookHolder
    {

        private static BookHolder instance;

        private BookHolder() { }
        
        //Синглетон
        public static BookHolder Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BookHolder();

                    if (true)
                    {
                        BookHolder.Book book = new BookHolder.Book();
                        book.Isbn = "1234-2345-3453-34534-1231";
                        book.Name = "The News Publisher Playbook";
                        book.Author = "Google Inc.";
                        book.Publisher = "Google Play Books";
                        book.Year = "23.03.2016";
                        book.Link = "https://books.google.by/books?id=O7T3CwAAQBAJ&hl=ru&source=gbs_slider_cls_metadata_7_mylibrary&redir_esc=y";
                        book.Price = "Free";
                        book.Description = "As a news publisher, you know that many readers are switching from print to their smartphones and tablets to access news.";
                        instance.AddBook(book);
                    }
                }
                return instance;
            }
        }

        private SortBy mSort = SortBy.Name;
        private List<Book> mBooks = new List<Book>();
        private List<OnDataLoadRequired> mListeners = new List<OnDataLoadRequired>();

        private static String mAdminName = "admin";
        private static String mAdminPassword = "apass";

        //Метод добавления книги
        public void AddBook(Book book)
        {
            if (!IsValidBook(book))
            {
                throw new InvalidBookException();
            }
            mBooks.Add(book);
            NotifyDataLoadRequired();
        }

        //Метод удаления книги
        public void RemoveBook(Book book)
        {
            mBooks.Remove(book);
            NotifyDataLoadRequired();
        }

        //Метод получения всех книг
        public List<Book> GetBooks()
        {
            switch (mSort)
            {
                case SortBy.Name:
                    mBooks.Sort(new NameCompare());
                    break;
                case SortBy.Author:
                    mBooks.Sort(new AuthorCompare());
                    break;
                case SortBy.Price:
                    mBooks.Sort(new PriceCompare());
                    break;
            }
            return mBooks;
        }

        public List<Book> Search(String query)
        {
            if (String.IsNullOrEmpty(query))
            {
                return GetBooks();
            }
            List<Book> outList = new List<Book>();
            foreach (Book book in mBooks)
            {
                if (book.Name.ToLower().Contains(query.ToLower()))
                {
                    outList.Add(book);
                }
            }
            return outList;
        }

        //Метод сохранения типа сортировки
        public void SetSortBy(SortBy sort)
        {
            mSort = sort;
            NotifyDataLoadRequired();
        }

        //Метод проверки валидности данных о книге
        public bool IsValidBook(Book book)
        {
            if (!IsYearValid(book.Year))
            {
                Debug.WriteLine("book year is not valid");
                return false;
            }
            if (!IsLinkValid(book.Link))
            {
                Debug.WriteLine("book link is not valid");
                return false;
            }
            Debug.WriteLine("Validate step end!");
            return !String.IsNullOrEmpty(book.Isbn)
                && !String.IsNullOrEmpty(book.Name)
                && !String.IsNullOrEmpty(book.Author)
                && !String.IsNullOrEmpty(book.Publisher)
                && !String.IsNullOrEmpty(book.Price)
                && !String.IsNullOrEmpty(book.Description);
        }

        //Метод регистрации слушателя
        public void RegisterListener(OnDataLoadRequired listener)
        {
            mListeners.Add(listener);
        }

        //Метод разрегестрирования слушателя
        public void UnregisterListener(OnDataLoadRequired listener)
        {
            mListeners.Remove(listener);
        }

        //Метод уведомления слушателей об изменение в списке
        public void NotifyDataLoadRequired()
        {
            foreach (OnDataLoadRequired listener in mListeners)
            {
                listener.OnDataLoadRequired();
            }
        }

        //Проверка валидности введенной ссылки
        public static bool IsLinkValid(string text)
        {
            return !String.IsNullOrEmpty(text) && (text.StartsWith("http://") || text.StartsWith("https://"));
        }

        //Проверка валидности введенной даты
        public static bool IsYearValid(string text)
        {
            return !String.IsNullOrEmpty(text) && text.Count(c => c == '.') == 2;
        }

        //Метод проверки имени пользователя и пароля
        public bool Login(string name, string password)
        {
            return mAdminName.Equals(name) && mAdminPassword.Equals(password);
        }

        //Класс, содержащий в себе всю основную информацию о книге
        public class Book
        {
            public string Isbn { get; set; }
            public string Name { get; set; }
            public string Author { get; set; }
            public string Year { get; set; }
            public string Publisher { get; set; }
            public string Link { get; set; }
            public string Price { get; set; }
            public string Description { get; set; }

            public ListViewItem ToListItem()
            {
                ListViewItem item = new ListViewItem(Isbn);
                ListViewItem.ListViewSubItemCollection subItems = item.SubItems;
                subItems.Add(Name);
                subItems.Add(Author);
                subItems.Add(Year);
                subItems.Add(Publisher);
                subItems.Add(Link);
                subItems.Add(Price);
                subItems.Add(Description);
                item.Tag = this;
                return item;
            }

        }

        //Класс сравнения книги по имени
        public class NameCompare : Comparer<Book>
        {
            public override int Compare(Book x, Book y)
            {
                return x.Name.CompareTo(y.Name);
            }
        }

        //Класс сравнения книги по автору
        public class AuthorCompare : Comparer<Book>
        {   
            public override int Compare(Book x, Book y)
            {
                return x.Author.CompareTo(y.Author);
            }
        }

        //Класс сравнения книги по цене
        public class PriceCompare : Comparer<Book>
        {
            public override int Compare(Book x, Book y)
            {
                return x.Price.CompareTo(y.Price);
            }
        }

        //Интерфейс для оповещений об изменениях в списке кнги
        public interface OnDataLoadRequired
        {
            void OnDataLoadRequired();
        }

        public enum SortBy
        {
            Name, Author, Price
        }

    }
}
