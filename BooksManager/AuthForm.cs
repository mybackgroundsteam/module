﻿using System;
using System.Windows.Forms;

namespace BooksManager
{
    public partial class AuthForm : Form
    {
        //Инициализация формы
        public AuthForm()
        {
            InitializeComponent();
        }

        //Обработка нажатия кнопки "войти"
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (BookHolder.Instance.Login(EdtName.Text, EdtPassword.Text))
            {
                Close();
            }
            else
            {
                MessageBox.Show("User name or password wrong :(");
            }
        }
    }
}
