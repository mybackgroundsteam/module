﻿using System;
using System.Windows.Forms;

namespace BooksManager
{
    public partial class AddBookDialog : Form
    {
        //Инициализация формы
        public AddBookDialog()
        {
            InitializeComponent();
        }

        //Обработка нажатия кнопки отмены
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        //Обработка нажатия кнопки сохранения
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (!isFieldsFilled())
            {
                MessageBox.Show("Fields can't be empty!");
            }
            else if (!BookHolder.IsLinkValid(EdtLink.Text))
            {
                MessageBox.Show("Link is not valid!");
            }
            else if (!BookHolder.IsYearValid(EdtYear.Text))
            {
                MessageBox.Show("Year not valid (shouuld be MM.DD.YYYY format)!");
            }
            else
            {
                BookHolder.Book book = new BookHolder.Book();
                book.Isbn = EdtIsbn.Text;
                book.Name = EdtName.Text;
                book.Author = EdtAuthor.Text;
                book.Year = EdtYear.Text;
                book.Publisher = EdtPublisher.Text;
                book.Price = EdtPrice.Text;
                book.Link = EdtLink.Text;
                book.Description = EdtDescription.Text;

                BookHolder.Instance.AddBook(book);
                Close();
            }
        }

        //Проверка заполенны ли поля
        private bool isFieldsFilled()
        {
            return !String.IsNullOrEmpty(EdtIsbn.Text)
                && !String.IsNullOrEmpty(EdtName.Text)
                && !String.IsNullOrEmpty(EdtAuthor.Text)
                && !String.IsNullOrEmpty(EdtYear.Text)
                && !String.IsNullOrEmpty(EdtPublisher.Text)
                && !String.IsNullOrEmpty(EdtPrice.Text)
                && !String.IsNullOrEmpty(EdtLink.Text)
                && !String.IsNullOrEmpty(EdtDescription.Text);
        }
        
    }
}
