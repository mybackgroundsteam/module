﻿namespace BooksManager
{
    partial class AddBookDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.EdtIsbn = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.EdtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.EdtAuthor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.EdtYear = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.EdtPublisher = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.EdtDescription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.EdtLink = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.EdtPrice = new System.Windows.Forms.TextBox();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ISBN";
            // 
            // EdtIsbn
            // 
            this.EdtIsbn.Location = new System.Drawing.Point(11, 24);
            this.EdtIsbn.Margin = new System.Windows.Forms.Padding(2);
            this.EdtIsbn.Name = "EdtIsbn";
            this.EdtIsbn.Size = new System.Drawing.Size(76, 20);
            this.EdtIsbn.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(88, 7);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Name";
            // 
            // EdtName
            // 
            this.EdtName.Location = new System.Drawing.Point(91, 24);
            this.EdtName.Margin = new System.Windows.Forms.Padding(2);
            this.EdtName.Name = "EdtName";
            this.EdtName.Size = new System.Drawing.Size(76, 20);
            this.EdtName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Author";
            // 
            // EdtAuthor
            // 
            this.EdtAuthor.Location = new System.Drawing.Point(12, 61);
            this.EdtAuthor.Margin = new System.Windows.Forms.Padding(2);
            this.EdtAuthor.Name = "EdtAuthor";
            this.EdtAuthor.Size = new System.Drawing.Size(76, 20);
            this.EdtAuthor.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(92, 45);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Year";
            // 
            // EdtYear
            // 
            this.EdtYear.Location = new System.Drawing.Point(92, 61);
            this.EdtYear.Margin = new System.Windows.Forms.Padding(2);
            this.EdtYear.Name = "EdtYear";
            this.EdtYear.Size = new System.Drawing.Size(76, 20);
            this.EdtYear.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 83);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Publisher";
            // 
            // EdtPublisher
            // 
            this.EdtPublisher.Location = new System.Drawing.Point(12, 99);
            this.EdtPublisher.Margin = new System.Windows.Forms.Padding(2);
            this.EdtPublisher.Name = "EdtPublisher";
            this.EdtPublisher.Size = new System.Drawing.Size(76, 20);
            this.EdtPublisher.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 161);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Description";
            // 
            // EdtDescription
            // 
            this.EdtDescription.Location = new System.Drawing.Point(12, 177);
            this.EdtDescription.Margin = new System.Windows.Forms.Padding(2);
            this.EdtDescription.Multiline = true;
            this.EdtDescription.Name = "EdtDescription";
            this.EdtDescription.Size = new System.Drawing.Size(156, 86);
            this.EdtDescription.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 121);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Link";
            // 
            // EdtLink
            // 
            this.EdtLink.Location = new System.Drawing.Point(12, 137);
            this.EdtLink.Margin = new System.Windows.Forms.Padding(2);
            this.EdtLink.Name = "EdtLink";
            this.EdtLink.Size = new System.Drawing.Size(156, 20);
            this.EdtLink.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(89, 83);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Price";
            // 
            // EdtPrice
            // 
            this.EdtPrice.Location = new System.Drawing.Point(92, 99);
            this.EdtPrice.Margin = new System.Windows.Forms.Padding(2);
            this.EdtPrice.Name = "EdtPrice";
            this.EdtPrice.Size = new System.Drawing.Size(76, 20);
            this.EdtPrice.TabIndex = 16;
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(92, 276);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(2);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(76, 27);
            this.BtnSave.TabIndex = 17;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(11, 275);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(76, 27);
            this.BtnCancel.TabIndex = 18;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // AddBookDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(178, 312);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.EdtPrice);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.EdtLink);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.EdtDescription);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.EdtPublisher);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.EdtYear);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.EdtAuthor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.EdtName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.EdtIsbn);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AddBookDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddBookDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox EdtIsbn;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox EdtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox EdtAuthor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox EdtYear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox EdtPublisher;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox EdtDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox EdtLink;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox EdtPrice;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnCancel;
    }
}