﻿using System;
using System.Windows.Forms;
using static BooksManager.BookHolder;

namespace BooksManager
{
    public partial class MainForm : Form, OnDataLoadRequired
    {
        //Инициализация формы
        public MainForm()
        {
            InitializeComponent();
            RadioSortName.Checked = true;
            new AuthForm().ShowDialog();
        }
        
        //Переопределенный метод загрузки формы
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            BookHolder.Instance.RegisterListener(this);
            LoadData();
        }

        //Переопределенный метод закрытия формы
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BookHolder.Instance.UnregisterListener(this);
        }

        //Метод, вызываемый в случае, если произошли изменения в данных, и необходимо их обновить
        public void OnDataLoadRequired()
        {
            LoadData();
        }

        //Метод загрузки данных
        private void LoadData()
        {
            BooksList.Items.Clear();
            foreach (Book book in BookHolder.Instance.GetBooks())
            {
                BooksList.Items.Add(book.ToListItem());
            }
        }
        
        //Обработка нажатия кнопок сортировки
        private void RadioSortClick(object sender, EventArgs e)
        {
            if (sender != RadioSortName)
            {
                RadioSortName.Checked = false;
            }
            else
            {
                BookHolder.Instance.SetSortBy(SortBy.Name);
            }
            if (sender != RadioSortAuthor)
            {
                RadioSortAuthor.Checked = false;
            }
            else
            {
                BookHolder.Instance.SetSortBy(SortBy.Author);
            }
            if (sender != RadioSortPrice)
            {
                RadioSortPrice.Checked = false;
            }
            else
            {
                BookHolder.Instance.SetSortBy(SortBy.Price);
            }
            ((RadioButton)sender).Checked = true;
        }

        //Обработка нажатия меню "Add book"
        private void AddBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AddBookDialog().ShowDialog();
        }

        //Обработка нажатия меню "Exit"
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //Обработка нажатия правой кнопки мыши на элементе списка
        private void OnListMouseClick(object sender, MouseEventArgs e)
        {
            ListView listView = sender as ListView;
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ListViewItem item = listView.GetItemAt(e.X, e.Y);
                if (item != null)
                {
                    item.Selected = true;
                    MenuItem deleteItem = new MenuItem("Delete book");
                    deleteItem.Click += new EventHandler(OnMenuItemDeleteClick);
                    deleteItem.Tag = item.Tag;
                    ContextMenu menu = new ContextMenu();
                    menu.MenuItems.Add(deleteItem);
                    menu.Show((Control) sender, new System.Drawing.Point(e.X, e.Y));
                }
            }
        }

        //Обработка нажатия пункта контекстного меню "Delete"
        private void OnMenuItemDeleteClick(object sender, EventArgs args)
        {
            MenuItem item = (MenuItem)sender;
            Book book = (Book)item.Tag;
            if (MessageBox.Show("Are you sure you want delete book \"" + book.Name + "\"?", "Delete book", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                BookHolder.Instance.RemoveBook(book);
            }
        }

        //Обработка нажатия пункта меню "Search"
        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new SearchForm().Show();
        }
    }
}
