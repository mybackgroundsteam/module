﻿namespace BooksManager
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BooksList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RadioSortName = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.RadioSortAuthor = new System.Windows.Forms.RadioButton();
            this.RadioSortPrice = new System.Windows.Forms.RadioButton();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BooksList
            // 
            this.BooksList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.BooksList.FullRowSelect = true;
            this.BooksList.Location = new System.Drawing.Point(9, 24);
            this.BooksList.Margin = new System.Windows.Forms.Padding(2);
            this.BooksList.MultiSelect = false;
            this.BooksList.Name = "BooksList";
            this.BooksList.Size = new System.Drawing.Size(471, 222);
            this.BooksList.TabIndex = 0;
            this.BooksList.UseCompatibleStateImageBehavior = false;
            this.BooksList.View = System.Windows.Forms.View.Details;
            this.BooksList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnListMouseClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Isbn";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Author";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Year";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Publisher";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Description";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Link";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Price";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addBookToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(541, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addBookToolStripMenuItem
            // 
            this.addBookToolStripMenuItem.Name = "addBookToolStripMenuItem";
            this.addBookToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.addBookToolStripMenuItem.Text = "Add book";
            this.addBookToolStripMenuItem.Click += new System.EventHandler(this.AddBookToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // RadioSortName
            // 
            this.RadioSortName.AutoSize = true;
            this.RadioSortName.Location = new System.Drawing.Point(484, 41);
            this.RadioSortName.Margin = new System.Windows.Forms.Padding(2);
            this.RadioSortName.Name = "RadioSortName";
            this.RadioSortName.Size = new System.Drawing.Size(53, 17);
            this.RadioSortName.TabIndex = 2;
            this.RadioSortName.TabStop = true;
            this.RadioSortName.Text = "Name";
            this.RadioSortName.UseVisualStyleBackColor = true;
            this.RadioSortName.Click += new System.EventHandler(this.RadioSortClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(482, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Sort by:";
            // 
            // RadioSortAuthor
            // 
            this.RadioSortAuthor.AutoSize = true;
            this.RadioSortAuthor.Location = new System.Drawing.Point(484, 63);
            this.RadioSortAuthor.Margin = new System.Windows.Forms.Padding(2);
            this.RadioSortAuthor.Name = "RadioSortAuthor";
            this.RadioSortAuthor.Size = new System.Drawing.Size(56, 17);
            this.RadioSortAuthor.TabIndex = 4;
            this.RadioSortAuthor.TabStop = true;
            this.RadioSortAuthor.Text = "Author";
            this.RadioSortAuthor.UseVisualStyleBackColor = true;
            this.RadioSortAuthor.Click += new System.EventHandler(this.RadioSortClick);
            // 
            // RadioSortPrice
            // 
            this.RadioSortPrice.AutoSize = true;
            this.RadioSortPrice.Location = new System.Drawing.Point(484, 84);
            this.RadioSortPrice.Margin = new System.Windows.Forms.Padding(2);
            this.RadioSortPrice.Name = "RadioSortPrice";
            this.RadioSortPrice.Size = new System.Drawing.Size(49, 17);
            this.RadioSortPrice.TabIndex = 5;
            this.RadioSortPrice.TabStop = true;
            this.RadioSortPrice.Text = "Price";
            this.RadioSortPrice.UseVisualStyleBackColor = true;
            this.RadioSortPrice.Click += new System.EventHandler(this.RadioSortClick);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.searchToolStripMenuItem.Text = "Search";
            this.searchToolStripMenuItem.Click += new System.EventHandler(this.searchToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 255);
            this.Controls.Add(this.RadioSortPrice);
            this.Controls.Add(this.RadioSortAuthor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RadioSortName);
            this.Controls.Add(this.BooksList);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Book Manager";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView BooksList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.RadioButton RadioSortName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton RadioSortAuthor;
        private System.Windows.Forms.RadioButton RadioSortPrice;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
    }
}

