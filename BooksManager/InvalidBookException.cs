﻿using System;

namespace BooksManager
{
    //Исключение добавления книги
    public class InvalidBookException : Exception
    {
        public override string Message
        {
            get
            {
                return "Error: book not valid!";
            }
        }
    }
}
