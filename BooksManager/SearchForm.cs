﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using static BooksManager.BookHolder;

namespace BooksManager
{
    public partial class SearchForm : Form, OnDataLoadRequired
    {

        private String mLastQuery;

        //Инициализация формы
        public SearchForm()
        {
            InitializeComponent();
        }

        //Переопределенный метод загрузки формы
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            BookHolder.Instance.RegisterListener(this);
            LoadData(null);
        }

        //Переопределенный метод закрытия формы
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            BookHolder.Instance.UnregisterListener(this);
        }

        //Метод, вызываемый в случае, если произошли изменения в данных, и необходимо их обновить
        public void OnDataLoadRequired()
        {
            LoadData(mLastQuery);
        }

        //Метод занрузки данных
        private void LoadData(string query)
        {
            mLastQuery = query;
            SearchResultList.Items.Clear();
            List<Book> books = BookHolder.Instance.Search(query);
            foreach (Book book in books)
            {
                SearchResultList.Items.Add(book.ToListItem());
            }
        }

        //Обработка изменений в поле ввода
        private void EdtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadData(((TextBox)sender).Text);
        }
    }
}
